import mechanicalsoup
import json

def fetch():
    # Connect to duckduckgo
    browser = mechanicalsoup.StatefulBrowser()
    browser.open("https://duckduckgo.com/")
    
    # Fill-in the search form
    browser.select_form('#search_form_homepage')
    browser["q"] = "MechanicalSoup"
    browser.submit_selected()
    
    # Display the results
    return {link.text: link.attrs['href'] for link in browser.get_current_page().select('a.result__a')}

def lambda_handler(event, context):
    # TODO implement
    return {
        "statusCode": 200,
        "body": json.dumps('Hello from Lambda!'),
        "text": json.dumps(fetch())
    }

if __name__ == "__main__":
    d = json.loads(lambda_handler(None, None)['text'])
    for i in d:
        print(i, '->', d[i])
